FROM osrf/ros:humble-desktop-full

#EXPOSE 1024 # needed for VS Code. Must be 1024 or higher

RUN apt-get update
RUN apt-get install -y git && apt-get install -y python3-pip

# Get the important/useful ros2 apps:
RUN apt-get install -y ros-humble-gazebo-*

RUN apt install -y ros-humble-cartographer
RUN apt install -y ros-humble-cartographer-ros 

RUN apt install -y ros-humble-navigation2
RUN apt install -y ros-humble-nav2-bringup

# Get the things that support the Turtlebot3
RUN apt install -y ros-humble-dynamixel-sdk
RUN apt install -y ros-humble-turtlebot3-*
# Useful for easy debugging
RUN apt install -y x11-apps

# Install VS Code
RUN apt install -y wget gpg
RUN wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
RUN install -D -o root -g root -m 644 packages.microsoft.gpg /etc/apt/keyrings/packages.microsoft.gpg
RUN sh -c 'echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'
RUN rm -f packages.microsoft.gpg
RUN apt install -y apt-transport-https
RUN apt update
RUN apt install -y code

# Set up robot user and create the home directory, etc
ARG USERNAME=robot
# RUN mkdir /home  # Unneccessary - file exists
RUN adduser --disabled-password --gecos "" $USERNAME && chown -R $USERNAME /home/$USERNAME

# Ensure sudo group users are not 
# asked for a password when using 
# sudo command by ammending sudoers file
RUN adduser $USERNAME sudo
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

WORKDIR /home/$USERNAME/

RUN echo 'export ROS_DOMAIN_ID=30 #TURTLEBOT3' >> ~/.bashrc
RUN export ROS_DOMAIN_ID=30 #TURTLEBOT3

RUN echo 'export TURTLEBOT3_MODEL=burger' >> ~/.bashrc
RUN export TURTLEBOT3_MODEL=burger

RUN echo 'export DONT_PROMPT_WSL_INSTALL=1' >> ~/.bashrc
RUN export DONT_PROMPT_WSL_INSTALL=1

RUN echo 'source /opt/ros/humble/setup.bash' >> ~/.bashrc

RUN wget https://git.exeter.ac.uk/drv201/csmm418/-/raw/master/docker/setup.sh

RUN mkdir /home/$USERNAME/ros2_ws
RUN chown -R $USERNAME:$USERNAME /home/$USERNAME/ros2_ws
RUN mkdir /home/$USERNAME/ros2_ws/src
RUN chown -R $USERNAME:$USERNAME /home/$USERNAME/ros2_ws/src
WORKDIR /home/$USERNAME/ros2_ws/src
RUN git clone https://git.exeter.ac.uk/drv201/csmm418_teleop.git
RUN mv /home/$USERNAME/ros2_ws/src/csmm418_teleop/dv_teleop/ /home/$USERNAME/ros2_ws/src/dv_teleop
RUN rm -rf /home/$USERNAME/ros2_ws/src/csmm418_teleop

RUN mkdir -p /home/$USERNAME/turtlebot3_ws/src
WORKDIR /home/$USERNAME/turtlebot3_ws/src
RUN git clone -b humble-devel https://github.com/ROBOTIS-GIT/turtlebot3_simulations.git
WORKDIR /home/$USERNAME/turtlebot3_ws

USER $USERNAME
