source /opt/ros/humble/setup.bash
mkdir -p turtlebot3_ws/src
cd turtlebot3_ws/src/
git clone https://git.exeter.ac.uk/drv201/csmm418_teleop.git
mv csmm418_teleop/dv_teleop/ dv_teleop
rm -rf csmm418_teleop/
sudo apt install ros-humble-turtlebot3-*
xeyes
cd ..
colcon build --symlink-install
rosdep update
rosdep install --from-path src --rosdistro humble
. install/setup.bash
sudo install/local_setup.bash
git clone -b humble-devel https://github.com/ROBOTIS-GIT/turtlebot3_simulations.git
. install/setup.bash
ros2 launch turtlebot3_gazebo empty_world.launch.py