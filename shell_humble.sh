source /opt/ros/humble/setup.bash 	#1. source ROS2
mkdir -p dvteleop_ws/src		#2. Create new directory and go there  
cd dvteleop_ws/src/
git clone https://git.exeter.ac.uk/drv201/csmm418_teleop.git 	#3. Get the code
mv csmm418_teleop/dv_teleop/ dv_teleop		#3a - these two lines are to get the
rm -rf csmm418_teleop/						#3b  package (dv_teleop) out of the git
cd ..
rosdep install -i --from-path src --rosdistro humble -y		#4. Resolve dependencies
colcon build --symlink-install								#5. Build

# Open a new terminal and:
# ------------------------
source /opt/ros/humble/setup.bash 		#6a. source ROS2 underlay
cd dvteleop_ws							#6b. Go to the workspace root
. install/local_setup.bash				#6c. Source the overlay

ros2 run dv_teleop listener				#6d. Run the package

# To modify code: do it in the first terminal, edit, then 4 and 5, then 
# come back to the second window and source overlay, underlay and run

#-------------- That's dv_teleop. Let's move on to turtlebot3_sim


cd ..
mkdir -p turtlebot3_ws/src		#2. Create new directory and go there  
cd turtlebot3_ws/src/
git clone -b humble-devel https://github.com/ROBOTIS-GIT/turtlebot3_simulations.git
cd ..
rosdep install -i --from-path src --rosdistro humble -y		#4. Resolve dependencies
colcon build --symlink-install

# New terminal
source /opt/ros/humble/setup.bash 		#6a. source ROS2 underlay
cd turtlebot3_ws							#6b. Go to the workspace root
. install/local_setup.bash				#6c. Source the overlay

ros2 launch turtlebot3_gazebo empty_world.launch.py				#6d. Run the package